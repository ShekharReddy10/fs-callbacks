const fs = require("fs");
const path = require("path");

function problem2() {
  fs.readFile(path.join(__dirname, "lipsum.txt"), (err, data) => {
    //Read data from lipsum.txt.
    if (err) {
      throw err;
    } else {
      console.log("Data read from lipsum.txt");
      data = data.toString().toUpperCase();
      fs.writeFile(path.join(__dirname, "./dataUpperCase.txt"), data, (err) => {
        //Converted data to UpperCase and written into dataUpperCase.txt
        if (err) {
          throw err;
        } else {
          console.log("Data converted to UpperCase");
          fs.writeFile(path.join(__dirname, "./filename.txt"), "dataUpperCase.txt", (err) => {
            // stored filename dataUpperCase to filename.txt
            if (err) {
              console.error(err);
            } else {
              console.log("dataUppercase.txt name is stored into filename.txt");
              fs.readFile(path.join(__dirname, "dataUpperCase.txt"), (err, upperCaseData) => {
                //Read data from newfile dataUpperCase.txt
                if (err) {
                  throw err;
                } else {
                  console.log("Read data from new file dataUpperCase.txt");
                  let toLowerCase = upperCaseData.toString().toLowerCase();
                  let toSplit = toLowerCase.split(". ");
                  let toSentences = toSplit.join(".\n");
                  fs.writeFile(
                    path.join(__dirname, "./dataLowerCaseAndSentences.txt"),
                    toSentences,
                    (err) => {
                      //converted into LowerCase with Sentences and written into dataLowerCaseAndSentences.txt
                      if (err) {
                        console.error(err);
                      } else {
                        console.log("Data converted to LowerCase Sentences");
                        fs.appendFile(path.join(__dirname, "./filename.txt"), "\ndataLowerCaseAndSentences.txt", (err) => {
                          //stored new file datLowerCaseAndSentences into filename.txt
                          if (err) {
                            console.error(err);
                          } else {
                            console.log("dataLowerCaseAndSentences.txt name is stored to filename.txt");
                            fs.readFile(path.join(__dirname, "dataLowerCaseAndSentences.txt"), (err, sentencedData) => {
                              //read data from dataLowerCaseAndSentences.txt
                              if (err) {
                                console.error(err);
                              } else {
                                console.log(
                                  "Read data from new file dataLowerCaseAndSentences.txt"
                                );
                                let joinSentence = sentencedData
                                  .toString()
                                  .split("\n");
                                let sortedData = joinSentence
                                  .sort()
                                  .slice(1);
                                fs.writeFile(
                                  path.join(
                                    __dirname,
                                    "sortedData.txt"
                                  ),
                                  JSON.stringify(sortedData),
                                  (err) => {
                                    //Sorted the data and written into sortedData.txt
                                    if (err) {
                                      console.err(err);
                                    } else {
                                      console.log("Data is sorted");
                                      fs.appendFile(
                                        path.join(
                                          __dirname,
                                          "./filename.txt"
                                        ),
                                        "\nsortedData.txt",
                                        (err) => {
                                          //Stored new file sortedData into filename.txt
                                          if (err) {
                                            console.error(err);
                                          } else {
                                            console.log(
                                              "sortedData.txt name is stored to filename.txt"
                                            );
                                            fs.readFile(
                                              path.join(
                                                __dirname,
                                                "./filename.txt"
                                              ),
                                              (err, fileNames) => {
                                                if (err) {
                                                  console.error(err);
                                                } else {
                                                  let files = fileNames
                                                    .toString()
                                                    .split("\n");
                                                  console.log(files);
                                                  files.forEach(
                                                    (file) => {
                                                      fs.unlink(
                                                        path.join(__dirname, file),
                                                        (err) => {
                                                          if (err) {
                                                            // Deleting all new files that stored in filename.txt
                                                            console.error(err);
                                                          } else {
                                                            console.log(`${file} is deleted`);
                                                          }
                                                        }
                                                      );
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                            );
                          }
                        }
                        );
                      }
                    }
                  );
                }
              }
              );
            }
          }
          );
        }
      });
    }
  });
}

module.exports = problem2;
