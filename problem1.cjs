const fs = require("fs");
const path = require("path");
const directory = path.join(__dirname, "./Random");

let create = (maxFiles) => {
  fs.mkdir(directory, function () {
    console.log("Random directory is created");
    let noOfFiles = Math.floor(Math.random() * maxFiles);
    console.log(`Number of Random files generated = ${noOfFiles}`);
    let createdFilesCount = 0;
    for (let countFile = 1; countFile <= noOfFiles; countFile++) {
      let array1 = [1, 2, 3];
      fs.writeFile(
        path.join(directory, `./file${countFile}.json`),
        JSON.stringify(array1),
        function (err) {
          createdFilesCount++;
          console.log(`file${countFile}.json added to Random directory`);
          if (createdFilesCount === noOfFiles) {
            console.log("Deleting...");
            for (let deleteFile = 1; deleteFile <= noOfFiles; deleteFile++) {
              fs.unlink(
                path.join(directory, `file${deleteFile}.json`),
                (error) => {
                  console.log(`file${deleteFile}.json is deleted`);
                  if (error) {
                    throw error;
                  }
                }
              );
            }
          }
        }
      );
    }
  });
};

module.exports = create;
